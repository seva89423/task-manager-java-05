PROJECT INFO
=====================
TASK MANAGER

DEVELOPER INFO
=====================
**NAME:** Vsevolod Zorin

**E-MAIL:** <seva89423@gmail.com>

STACK OF TECHNOLOGIES
=====================
- Java Development Kit v.1.8
- Maven
- IntelliJ IDEA 2020 1.3
- Git Bash 

SOFTWARE
=====================
- JDK 1.8
- MS WINDOWS 10 / MAC OS Catalina

PROGRAM BUILD
=====================
```
mvn clean package
```

PROGRAM RUN
=====================
```
java -jar ./task-manager-1.0.1.jar
```
